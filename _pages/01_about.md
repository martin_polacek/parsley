---
layout: page
title: About
permalink: /about/
---
<img src="/pictures/ja.JPG" alt="Martin Polacek" width="255px" >

My name is Martin Polacek. I like math, physics and programming. Currently
I am Ph.D. student at the [YITP][yitp] institute for theoretical
physics at the [Stony Brook University][sb], [Stony Brook][sbrook], NY.

### Trivia

I have been born in the city of [Zilina][zln], [Slovakia][svk]. I obtained the
Master Degree (MSc.) in Theoretical Physics at the [Comenius University][cmn] in
[Bratislava][bts]. After that I moved to [New York City][nyc], where I live since then.

### Resume

You can read more about my scientific and professional interests in my [Resume][resume].



[me]: /pictures/ja.JPG "Martin Polacek"
[zln]: https://en.wikipedia.org/wiki/�ilina "Zilina"
[svk]: https://en.wikipedia.org/wiki/Slovakia "Slovakia"
[cmn]: http://uniba.sk/en/ "Comenius_University"
[bts]: https://en.wikipedia.org/wiki/Bratislava "Bratislava"
[nyc]: https://en.wikipedia.org/wiki/New_York_City "NYC"
[yitp]: http://insti.physics.sunysb.edu/itp/www/ "YITP"
[sb]: http://www.stonybrook.edu "Stony Brook University"
[sbrook]: https://en.wikipedia.org/wiki/Stony_Brook,_New_York "Stony Brook"
[resume]: /pdf/Martin_Polacek_Resume.pdf "Msartin Polacek Resume"
