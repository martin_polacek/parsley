---
layout: page
title: Code
permalink: /code/
---
My main CS interests are:

* Machine Learning (AI) and Data Analysis (Hadoop)
* Application of Recurrent Neural Networks
* Application of Graphical Models
* Graph algorithms
* Advanced programming problems, TSP etc.
* Web development, Ruby on Rails, jekyll etc.

### Projects

I have been involved in various CS projects:

1. [hhh][ggl]
2. 

[ggl]: /welcome/ "welcome_blog"
